#include "cicular_buffer.h"


unsigned char buffer[BUFFER_SIZE];
short pointer_push = 0;
short pointer_pop = 0;
uint8_t free_space = BUFFER_SIZE;

void buffer_push(unsigned char midi_signal){
	if(free_space){
		buffer[pointer_push] = midi_signal;
		--free_space;
		pointer_push = (pointer_push +1) % BUFFER_SIZE;

	}

}

unsigned char buffer_pop(){
	if(free_space != BUFFER_SIZE){
		unsigned char midi_signal = buffer[pointer_pop];
		pointer_pop = (pointer_pop +1) % BUFFER_SIZE;
		++free_space;
		return 	midi_signal;
	}
	return 0x00;
}

uint8_t get_free_space(){
	return free_space;
}

