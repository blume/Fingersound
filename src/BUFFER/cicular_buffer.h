#ifndef BUFFER_CICULAR_BUFFER_H_
#define BUFFER_CICULAR_BUFFER_H_
#include <stdint.h>

/*
 * qeue like cicular buffer
 * first in first out */
#define BUFFER_SIZE 9

void buffer_push(unsigned char midi_signal);
unsigned char buffer_pop();
uint8_t get_free_space();


#endif /* BUFFER_CICULAR_BUFFER_H_ */
