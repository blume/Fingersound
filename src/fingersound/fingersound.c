
#include <avr/io.h>
#include <util/delay.h>
#include <avr/interrupt.h>
#include <stdint.h>
#include <string.h>

#include "uart.h"
#include "timer.h"
#include "adc.h"
#include "io_ports.h"
#include "cicular_buffer.h"



// Quarz with 8 Mhz prescaler = 8
//#define F_CPU 1000000UL




//Unused use these to implement buttons for change octave & Co
#define ctrlButPlus		0b00000001
#define ctrlButMinus	0b00000010
#define ctrlButEnter	0b00000100

#define FINGERS ((~PINC) & 0x0F)
#define CONTROLS ((~PINB) & 0x07)



#define NoneFinger		0b00000000 //neither contact is pressed
#define ForeFinger		0b00000001
#define ForeFingerAnd	0b00000011
#define MiddleFinger	0b00000010
#define MiddleFingerAnd 0b00000110
#define RingFinger		0b00000100
#define RingFingerAnd	0b00001100
#define PinkyFinger		0b00001000

#define NOTE_OFF 0x80
#define NOTE_ON 0x90

// mapping ADC (10bit) to midi velocity (8bit)
// (uint8_t) (ADC >> 3) +1 would also be possible -> ignore 3 lowest bits
// test both possibilities
#define VELOCITY (uint8_t) (ADC /8.1 + 1)

//Define notes to access them easier later on
//All defined notes are on the lowest octave
#define C  0x01 //1st  note //middle C is 0x3C
#define Cs 0x02 //2nd  note //middle C# is 0x3D
#define D  0x03 //3rd  note //middle D is 0x3E
#define Ds 0x04 //4th  note //middle D# is 0x3F
#define E  0x05 //5th  note //middle E is 0x40
#define F  0x06 //6th  note //middle F is 0x41
#define Fs 0x07 //7th  note //middle F# is 0x42
#define G  0x08 //8th  note //middle G is 0x43
#define Gs 0x09 //9th  note //middle G# is 0x44
#define A  0x0A //10th note //middle A is 0x45
#define As 0x0B //11th note //middle A# is 0x46
#define H  0x0C //12th note //middle H is 0x47

#define TRUE 1
#define FALSE 0

short send = FALSE;

//defines scale we are currently in
enum scale {major, minor};
enum scale assigned_scale = 0;
//unsigned char assigned_scale = scale.major;


//new enum to be tested
enum function {scaling, octaving};
enum function selected_function = 0;


unsigned char last_tone = 0x00;
unsigned char last_fingers = 0x00;
unsigned char unchecked_fingers = 0x00;
unsigned char last_controls = 0x00;
unsigned char unchecked_controls = 0x00;

// devine octave to play on
unsigned char octave = 0x0C;
unsigned char octave_mult = 0x05;
//offset from "original" note
unsigned char note_pitch = 0x00;

/*
Major and Minor scales

Notes should be placed in order from first forefinger to your pinky, including two fingers combinations:

1. Forefinger					> first note (e.g. C)
2. Forefinger + Middle Finger	> second note (e.g. D)
3. Middle Finger				> third note (e.g. E)
4. Middle- + Ring Finger		> fourth note (e.g. F)
5. Ring Finger					> fifth note (e.g. G)
6. Ring Finger + Pinky			> sixt note (e.g. A)
7. Pinky						> seventh note (e.g. H)

*/
unsigned char majorScale[7] = {C,D,E,F,G,A,H};			//should these scales remain unsigned?
unsigned char minorScale[7] = {C,D,Ds,F,G,Gs,As};
// defined in init
unsigned char currentScale[7];


enum scale get_scale(){
	return assigned_scale;
}
void assignNewScale(unsigned char newScale[7]){
	for (int i = 0; i < 7; ++i) {
		currentScale[i] = newScale[i];
	}
}

void changeScale()
{	
	switch(assigned_scale){
		case major: assignNewScale(majorScale);
			assigned_scale = minor;
		break;
		
		case minor: assignNewScale(minorScale);
			assigned_scale = major;
		break;
		
		default: assignNewScale(majorScale); 
			assigned_scale = minor;
		break;
	}
}

void increaseOctave(){
	if(octave_mult < 8)
		octave_mult += 1;
}

void decreaseOctave(){
	if(octave_mult > 2)
	octave_mult -= 1;
}

void play_node(unsigned char tone){

	//for testing set to ADC later
	unsigned char velocity = 0x7F;
	//next_byte = STATUSBYTE;
	if(last_tone){
			buffer_push(NOTE_OFF);
			buffer_push(last_tone);
			buffer_push(velocity);
	}
	if(tone){
		buffer_push(NOTE_ON);
		buffer_push(tone);
		buffer_push(VELOCITY);
	}
	last_tone = tone;
}

//new function to be tested
void changeFunction(){
	switch (selected_function){
		case scaling: selected_function = octaving; break;
		case octaving: selected_function = scaling; break;
		default: selected_function = 0; break;
	}

}
void processFingers(uint8_t checked_fingers){
	unsigned char scaler = octave * octave_mult + note_pitch - 0x01;
	switch (checked_fingers){
		case NoneFinger		: play_node(0x00);break;
		case ForeFinger		: play_node(currentScale[0] + scaler) ;break;
		case ForeFingerAnd	: play_node(currentScale[1] + scaler);break;
		case MiddleFinger	: play_node(currentScale[2] + scaler);break;
		case MiddleFingerAnd: play_node(currentScale[3] + scaler);break;
		case RingFinger		: play_node(currentScale[4] + scaler);break;
		case RingFingerAnd	: play_node(currentScale[5] + scaler);break;
		case PinkyFinger	: play_node(currentScale[6] + scaler);break;
		default : break; //play_node(0x00, NOTE_OFF);break; maybe?
	}
	last_fingers = checked_fingers;
}
void processControls(uint8_t checked_controls){
	switch(checked_controls){
		case ctrlButEnter	: changeFunction();break;
		case ctrlButPlus	: selected_function == scaling ? changeScale() : increaseOctave();break;
		case ctrlButMinus	: selected_function == scaling ? changeScale() : decreaseOctave();break;
		default : break;
	}
	last_controls = checked_controls;

}
// quick and dirty changed variable names and create defines for port/pin and mask
void processInput(){
	if(FINGERS != last_fingers){
		unchecked_fingers = FINGERS;
		if(debounce(&PINC, 0x0F, unchecked_fingers)){
			processFingers(unchecked_fingers);
		}
	}
	if(CONTROLS != last_controls){
//		uart_putc(last_controls);
		unchecked_controls = CONTROLS;
		if(debounce(&PINB, 0x07, unchecked_controls)){
			processControls(unchecked_controls);
		}
	}
}

void init(){
	//WAIT UNTIL SYSTEM START
	//Read in datasheet how long capacitor need to load
	//....

	assignNewScale(minorScale);
	//DISABLE ALL INTERRUPTS
	cli();

	//INITIALISATION
	//F_CPU defined in begin of file / Productive BAUD Rate initialized in uart.h
	uart_init( UART_BAUD_SELECT(UART_PRODUCTIVE_BAUD_RATE, F_CPU) );

	io_ports_init();
	adc_init();
	adc_start();
	init_timer();
	init_debounce_timer(30);

	//F_CPU defined in begin of file / Productive BAUD Rate initialized in uart.h
	uart_init( UART_BAUD_SELECT(UART_PRODUCTIVE_BAUD_RATE, F_CPU) );

	//(1000000/BUDRATE)*10(anzahl bits pro übertragung)
	//only works because 1 timer_cycle is 1 micro_sec since micro controller runs with 1 Mhz
	set_timer_cycles(2084);


	//enable all  interrupts
	sei();
}

int main(void){

	init();
	while (1)
	{
		if(get_free_space() == ((uint8_t)BUFFER_SIZE) ){
					processInput();
		}
		else{
			if(get_timer_overflow()){
					uart_putc(buffer_pop());
					set_timer_cycles(2084);
					reset_timer_overflow();
			}
		}
	}
	//never reached
	return 0;
}
