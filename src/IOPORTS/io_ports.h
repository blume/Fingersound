#ifndef _IO_PORTS_H_
#define _IO_PORTS_H_

void io_ports_init();
int debounce(volatile uint8_t *port, uint8_t mask, uint8_t value);

#endif /* IO_PORTS_H_ */
