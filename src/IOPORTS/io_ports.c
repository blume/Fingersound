#include <avr/io.h>

#include "io_ports.h"
#include "timer.h"
#include "uart.h"

uint8_t port_to_debounce;
uint8_t last_value_of_port;

void io_ports_init(){
	//	portb as input
		DDRB = 0;
		//portc as input
		DDRC = 0;
		// Disable digital input on PORTC5 (reserved for ADC)
		DIDR0 = (1 << ADC5D);
}

int debounce(volatile uint8_t *port, uint8_t mask, uint8_t value){
	switch (get_debounce_timer_status()) {
	case DEBOUNCE_TIMER_OVERFLOW: {
		stop_and_clear_debounce_timer();
		return (port_to_debounce == *port) && (last_value_of_port == value);
	}
		break;
	case DEBOUNCE_TIMER_OFF: {
		port_to_debounce = *port;
		last_value_of_port = value;
		start_debounce_timer();
		return 0;
	}
		break;
	case DEBOUNCE_TIMER_RUNNING:
		return 0;
		break;
	default:
		return 0;
		break;
	}
}
