#include <stdint.h>

#ifndef _TIMER_H_
#define _TIMER_H_


#define DEBOUNCE_TIMER_OFF 0
#define DEBOUNCE_TIMER_RUNNING 1
#define DEBOUNCE_TIMER_OVERFLOW 2

void init_timer();
void init_debounce_timer(uint16_t milliseconds);
void start_debounce_timer();
void set_timer_cycles(int mc_cycles);
void stop_and_clear_debounce_timer();
void reset_timer_overflow();
short get_timer_overflow();
short get_debounce_timer_status();


#endif /* _TIMER_H_ */
