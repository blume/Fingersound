#include "timer.h"
#include "uart.h"
#include <avr/io.h>
#include <avr/interrupt.h>

#define DEBOUNCE_TIMER_OVERFLOW_VALUE (uint8_t) 250

uint8_t max_overflows = 0xFF;
uint8_t overflow_counter = 0;

short timer_overflow;
short debounce_timer_status;

void reset_timer_overflow(){
	timer_overflow = 0;
}
short get_timer_overflow(){
	return timer_overflow;
}

short get_debounce_timer_status(){
	return debounce_timer_status;
}
void init_timer(){
	timer_overflow = 0;
	//Set Timer to Clear Timer on Compare Match (CTC) mode, compare with OCR1A
	TCCR1A = (0 << WGM13) | (1 << WGM12) | (0 << WGM11) | (0 << WGM10);
	// Normal port operation OCR1A (TOP value) isn't reset
	TCCR1A |= (0 << COM1A1) |  (0 << COM1A0);
	// Set prescaler to 1 (no presacler)
	TCCR1B = (0 << CS12) | (0 << CS11) | (1<<CS10);
}
void init_debounce_timer(uint16_t milliseconds){
	debounce_timer_status = DEBOUNCE_TIMER_OFF;
	max_overflows = (1000 * milliseconds) / DEBOUNCE_TIMER_OVERFLOW_VALUE;
	//Set Timer to Clear Timer on Compare Match (CTC) mode, compare with OCR0A
	TCCR0A = (0 << WGM02) | (1 << WGM01) | (0 << WGM00);
	// Set prescaler to 0 (stop timer)
	TCCR0B = (0 << CS02) | (0 << CS01) | (0 << CS00);
	//set timer start value
	TCNT0 = 0x00;
	// set max value for timer
	OCR0A = DEBOUNCE_TIMER_OVERFLOW_VALUE;
	//enable interrupt when counter1 matches OCR1A(Top vale for counter)
	TIMSK0 = (1 << OCIE0A);
}

void start_debounce_timer(){
	/*enable interrupts and flags*/

		//enable interrupt flag, for match of counter and OCR1A
	//TIFR0 = (1 << OCF0B);
	//set timer start value
	TCNT0 = 0x00;
	// start timer with prescaler = 1 (no presacleing)
	TCCR0B = (0 << CS02) | (0 << CS01) | (1 << CS00);
	debounce_timer_status = DEBOUNCE_TIMER_RUNNING;
}

void stop_and_clear_debounce_timer(){
	TCCR0B = (0 << CS02) | (0 << CS01) | (0 << CS00);
	TCNT0 = 0x00;
	debounce_timer_status = DEBOUNCE_TIMER_OFF;
	overflow_counter = 0;
}
void set_timer_cycles(int mc_cycles){
	//set timer to zero, because timer starts after init
	TCNT1 = 0x00;

	//set TOP value for counter
	OCR1A = mc_cycles;

	/*enable interrupts and flags*/
	//enable interrupt when counter1 matches OCR1A(Top vale for counter)
	TIMSK1 = (1 << OCIE1A);
	//enable interrupt flag, for match of counter and OCR1A
	TIFR1 = (1 << OCF1A);
}

ISR(TIMER1_COMPA_vect){
	timer_overflow = 1;
}

ISR(TIMER0_COMPA_vect){
	TCNT0 = 0x00;
	if(overflow_counter == max_overflows){
		overflow_counter = 0;
		debounce_timer_status = DEBOUNCE_TIMER_OVERFLOW;
	}
	else{
		overflow_counter++;
	}
}
