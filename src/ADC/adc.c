#include <avr/io.h>

#include "adc.h"

void adc_init(){
	//use AVcc as reference voltage
		ADMUX = (0 << REFS1) | (1 << REFS0);
		//use ADC5 (digital power, if analog power is needed use adc <= 3 (0011))
		ADMUX |= (0 << MUX3) | (1 << MUX2) | (0 << MUX1) | (1 << MUX0);

		//Enable Auto Trigger(ADATE) and enable ADC (ADEN)
		ADCSRA =  (1 << ADATE) | (1 << ADEN);
		//Set ADC to Free running mode
		ADCSRB = (0 << ADTS0) | (0 << ADTS1) | (0 << ADTS2) ;
}

void adc_start(){
	//Start ADC
	ADCSRA |= (1 << ADSC);
}

void adc_to_portb(){
	PORTB = ADC;
}
