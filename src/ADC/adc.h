#ifndef ADC_ADC_H_
#define ADC_ADC_H_

void adc_init();
void adc_start();
// only for debugging
void adc_to_portb();

#endif /* ADC_ADC_H_ */
