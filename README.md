# Fingersound
<img src="./readme_resources/fingersound.png" width="40%">

This Glove you can use as a MIDI-Instrument in your favorite DAW (e.g. LMMS, Ableton, Fruty Loops...).
You can Play in C major and Minor scale and in different octaves.
A demo video is in the readme_reasources folder.\
Feel Free to hack and send a pull request if you like. 

# Build
## Hardware
### Materials
* Glove
* Micro-USB to USB-A to connect the FTDI chip to a PC
* The components from the circuit diagram
* Solder, soldering-gun etc.

Connect everything like in the circuit diagram below.

![](./circuit_diagramms/fingersound_circuit.png)

## Software
### Install dependencies
#### Fedora
sudo dnf install avr-binutils avr-gcc avr-gcc-c++ avr-libc avr-libc-doc avra avrdude

### Build and upload to AVR microcontroller
Install [Eclipse CDT](https://www.eclipse.org/cdt/) and the [AVR-Plugin](https://marketplace.eclipse.org/content/avr-eclipse-plugin), import files and build and install with eclipse.

[Detailed manual](https://www.instructables.com/How-to-get-started-with-Eclipse-and-AVR/) on how to setup Eclipse for AVR programming.

Make sure your microcontroller is clocked with 1Mz if you use the 8Mhz quartz crystal like I do set the prescaler to 8. 

### Other build options
Feel free to create a makefile and program the microcontroller via the avrdude commandline tool.
Maybe I a instructions some time, but no guarantee.

# Usage

## Serial-MIDI bridge
Since the Fingersound-Glove is sending serial signals you need to install a serial-midi bridge on your pc to use it as midi device.
After you setting up the midi bridge you should find a new midi device/channel in your DAW

### Linux
Install [ttymidi](http://www.varal.org/ttymidi/).
To get the serial port on which the glove is connected use:\
sudo dmesg | grep FTDI.*ttyUSB

Use the resulting ttyUSB port (here ttyUSB0) in the ttymidi command to run the bridge.\
ttymidi -b 4800 -s /dev/ttyUSB0

If you want to use a GUI application use hairless-midiserial but it depends on some old 32bit libraries and it could be a pain to get it running

### Windows

Install and setup [hairless-midiserial](http://projectgus.github.io/hairless-midiserial/?source=post_page---------------------------) you can find the right serial port via the device manager.
Since I don't use Windows I can't give an detailed explanation.

## How to play

![](./readme_resources/usage.png)

1. GND + pressure sensor
2. Connectors
3. Control button 1 (octave down / change scale)
4. Control button 2 (octave up / change scale)
5. Control button 3 (toggle mods of button 1 and 2)

Pressing fingers on your thumb to play the following notes\
![](./readme_resources/note_table.png)

# Further development ideas
* change from copper contacts on the fingers to conducting textile.
* add more scales
* program a real driver were users can easily modify which note is played which which finger







